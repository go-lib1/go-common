package pdf

import (
	"testing"
)

var contentString = [][]string{
	{"旗舰店12月入库商品列表"},
	{"商品Id", "商品名称", "商品库存"},
	{"1", "r:商品1", "g:1"},
	{"2", "r:商品2", "g:2"},
	{"3", "r:商品3", "g:3"},
}

func Test_pdf(t *testing.T) {
	direction := Horizontal
	contentCell := AdaptCell(direction, contentString, 0, 0)
	conf := NewPdfConfig(direction, contentString, 0)
	if err := conf.CreatePdf(10, 10, "xxxxxxx/pkg/pdf/songsTi.ttf ", "test.pdf", contentCell); err != nil {
		t.Error(err)
	}
}
