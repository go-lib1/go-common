package pdf

import (
	"fmt"
	"github.com/gogf/gf/v2/text/gstr"
	"github.com/samber/lo"
	"github.com/signintech/gopdf"
	"path/filepath"
	"runtime"
	"strings"
)

const (
	Horizontal = "horizontal"
	Vertical   = "vertical"
)

/*
var contentCells = []*PdfCell{
	{Titles: []string{"旗舰店12月入库商品列表"}, Ws: []float64{800}, H: 50},
	{Titles: []string{"商品Id", "商品名称", "商品库存"}, Ws: []float64{70, 660, 70}, H: 40},
	{Titles: []string{"1", "商品1", "1"}, Ws: []float64{70, 660, 70}, H: 40},
	{Titles: []string{"2", "商品2", "2"}, Ws: []float64{70, 660, 70}, H: 40},
	{Titles: []string{"3", "商品3", "3"}, Ws: []float64{70, 660, 70}, H: 40},
}

var contentString = [][]string{
	{"旗舰店12月入库商品列表"},
	{"商品Id", "商品名称", "商品库存"},
	{"1", "商品1", "1"},
	{"2", "商品2", "2"},
	{"3", "商品3", "3"},
}
*/

type PdfCell struct {
	Titles []string
	Ws     []float64
	H      float64
}

func AdaptCell(direction string, content [][]string, w, h float64) []*PdfCell {
	//W: 595, H: 842
	if len(direction) == 0 {
		direction = Horizontal
	}

	if direction == Horizontal {
		w = lo.Ternary(w == 0, 822, w)
		h = lo.Ternary(h == 0, 25, h)
	} else if direction == Vertical {
		w = lo.Ternary(w == 0, 575, w)
		h = lo.Ternary(h == 0, 25, h)
	}

	var pdfCells []*PdfCell
	for _, sli := range content {
		cell := &PdfCell{}
		length := len(sli)
		for j := 0; j < length; j++ {
			cell.Titles = append(cell.Titles, sli[j])
			cell.Ws = append(cell.Ws, w/float64(length))
		}
		cell.H = h

		pdfCells = append(pdfCells, cell)
	}
	return pdfCells
}

type PdfConifg struct {
	size     int
	font     string
	align    gopdf.CellOption
	color    string
	pageSize gopdf.Rect
}

func NewPdfConfig(direction string, content [][]string, width float64) *PdfConifg {
	return &PdfConifg{
		size:     11,
		font:     "HDZB_5",
		align:    gopdf.CellOption{Align: gopdf.Center | gopdf.Middle, Border: gopdf.Left | gopdf.Right | gopdf.Bottom | gopdf.Top},
		color:    "#000000",
		pageSize: *getPageRect(direction, content, width),
	}
}

func getPageRect(direction string, content [][]string, width float64) *gopdf.Rect {
	//W: 595, H: 842
	if len(direction) == 0 {
		direction = Horizontal
	}

	var rect *gopdf.Rect
	if direction == Horizontal {
		row := len(content)
		if row > 23 {
			row = 23
		}

		h := 20 + 25*float64(row)
		rect = &gopdf.Rect{W: lo.Ternary(width == 0, 842, width+20), H: h}
	} else if direction == Vertical {
		row := len(content)
		if row > 32 {
			row = 32
		}

		h := 20 + 25*float64(row)
		rect = &gopdf.Rect{W: lo.Ternary(width == 0, 595, width+20), H: h}
	}

	rect.UnitsToPoints(gopdf.UnitPT)
	return rect
}

func (p *PdfConifg) SetSize(size int) *PdfConifg {
	p.size = size
	return p
}

func (p *PdfConifg) SetFont(font string) *PdfConifg {
	p.font = font
	return p
}

func (p *PdfConifg) SetAlign(align gopdf.CellOption) *PdfConifg {
	p.align = align
	return p
}

func (p *PdfConifg) SetColor(color string) *PdfConifg {
	p.color = color
	return p
}

func (p *PdfConifg) SetPageSize(pageSize gopdf.Rect) *PdfConifg {
	p.pageSize = pageSize
	return p
}

func (p *PdfConifg) CreatePdf(x, y float64, ttfPath, output string, contentCells []*PdfCell) error {
	if len(contentCells) == 0 {
		return fmt.Errorf("contentCells is empty")
	}

	pdf := &gopdf.GoPdf{}
	pdf.Start(gopdf.Config{PageSize: p.pageSize})
	pdf.AddPage()

	trimTtfPath := gstr.Trim(ttfPath)
	path := lo.Ternary(len(trimTtfPath) > 0, trimTtfPath, getTTFPath())
	if err := pdf.AddTTFFont(p.font, path); err != nil {
		return err
	}

	if err := pdf.SetFont(p.font, "", p.size); err != nil {
		return err
	}

	pdf.SetTextColor(hexToRGB(p.color))

	for _, v := range contentCells {
		titleX := x
		currY := float64(0)
		for i, w := range v.Ws {
			text := parseTextColor(pdf, v.Titles[i])
			pdf.SetX(titleX)
			pdf.SetY(y)
			pdf.CellWithOption(&gopdf.Rect{
				W: w,
				H: v.H,
			}, text, p.align)
			titleX += w
			currY += lo.Ternary(i == 0, v.H, 0)
		}
		y += currY
	}

	//save to file
	if err := pdf.WritePdf(output); err != nil {
		return err
	}
	return nil
}

func parseTextColor(pdf *gopdf.GoPdf, title string) string {
	if strings.HasPrefix(title, "r:") {
		pdf.SetTextColor(255, 0, 0)
		return title[2:]
	} else if strings.HasPrefix(title, "g:") {
		pdf.SetTextColor(0, 255, 0)
		return title[2:]
	} else {
		pdf.SetTextColor(0, 0, 0)
		return title
	}
}

func hexToRGB(hex string) (r, g, b uint8) {
	fmt.Sscanf(hex, "#%02x%02x%02x", &r, &g, &b)
	return r, g, b
}

func getTTFPath() string {
	// 获取当前文件的绝对路径
	_, filename, _, _ := runtime.Caller(0)

	// 获取当前文件所在的目录
	dir := filepath.Dir(filename)

	return filepath.Join(dir, "songsTi.ttf")
}
