package log

import (
	"github.com/sirupsen/logrus"
	"sync"
)

type logManager struct {
	logger *logrus.Logger
	once   sync.Once
}

var _lm = &logManager{}

type Fields = logrus.Fields

func Log() *logrus.Logger {
	_lm.once.Do(func() {
		_lm.logger = newLogger()
	})

	return _lm.logger
}

func newLogger() *logrus.Logger {
	log := logrus.New()

	log.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})
	log.SetLevel(logrus.InfoLevel)

	return log
}
