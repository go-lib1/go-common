package db

import (
	"context"
	"fmt"
	"testing"
	"time"
)

func TestDBTx(t *testing.T) {
	val := 1
	ctx := context.WithValue(context.Background(), "tx", val)

	for i := 0; i < 3; i++ {
		printWithCtx(ctx)
		time.Sleep(time.Second)
		subPrintWithCtx(ctx)
	}

	ctx = context.TODO()
	for i := 0; i < 3; i++ {
		printWithCtx(ctx)
		time.Sleep(time.Second)
		subPrintWithCtx(ctx)
	}

	ctx = context.WithValue(context.Background(), "tx", "abc")

	for i := 0; i < 3; i++ {
		printWithCtx(ctx)
		time.Sleep(time.Second)
		subPrintWithCtx(ctx)
	}

	ctx = context.TODO()

}

func printWithCtx(ctx context.Context) {
	fmt.Println(ctx.Value("tx"))
}

func subPrintWithCtx(ctx context.Context) {
	fmt.Println("sub: ", ctx.Value("tx"))
}
