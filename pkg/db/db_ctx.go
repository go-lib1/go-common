package db

import (
	"context"
	"gorm.io/gorm"
)

func GetDBWithCtx(ctx context.Context) *gorm.DB {
	if ctx == nil {
		return _db
	}

	txValue := ctx.Value("tx")
	if txValue != nil {
		if txDb, ok := txValue.(*gorm.DB); ok {
			return txDb
		} else {
			return _db
		}
	}
	return _db
}

type TxClient struct {
	Tx  *gorm.DB
	Ctx context.Context
}

func Begin() *TxClient {
	tx := GetDB().Begin()
	return &TxClient{
		Tx:  tx,
		Ctx: Tx(tx),
	}
}

func (t *TxClient) Commit() {
	if t != nil {
		t.Tx.Commit()
		t.Ctx = context.TODO()
	}
}

func (t *TxClient) Rollback() {
	if t != nil {
		t.Tx.Rollback()
		t.Ctx = context.TODO()
	}
}

func Tx(val any) context.Context {
	if tx, ok := val.(*gorm.DB); ok {
		return context.WithValue(context.Background(), "tx", tx)
	}
	return context.TODO()
}
