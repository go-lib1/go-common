package db

import (
	"context"
	"gorm.io/gorm"
)

var (
	_db  *gorm.DB
	_db1 *gorm.DB
	_db2 *gorm.DB
)

func SetDB(db *gorm.DB) {
	_db = db
}

func GetDB() *gorm.DB {
	return _db
}

func SetDBWithIdx(db *gorm.DB, idx int) {
	switch idx {
	case 1:
		_db1 = db
	case 2:
		_db2 = db
	}
}

func GetDBWithIdx(idx int) *gorm.DB {
	switch idx {
	case 1:
		return _db1
	case 2:
		return _db2
	}
	return nil
}

func Creates[T any](ctx context.Context, data []*T) error {
	return GetDBWithCtx(ctx).Create(data).Error
}

func Create[T any](ctx context.Context, data *T, query any, args ...any) error {
	return GetDBWithCtx(ctx).Where(query, args...).Create(data).Error
}

func FirstOrCreate[T any](ctx context.Context, data *T, query any, args ...any) error {
	return GetDBWithCtx(ctx).Where(query, args...).FirstOrCreate(data).Error
}

func AssignCreate[T any](ctx context.Context, assign *T, query any, args ...any) error {
	tmp := new(T)
	return GetDBWithCtx(ctx).Where(query, args...).Assign(assign).FirstOrCreate(tmp).Error
}

func Updates[T any](ctx context.Context, value any, query any, args ...any) error {
	return GetDBWithCtx(ctx).Model(new(T)).Where(query, args...).Updates(value).Error
}

func Update[T any](ctx context.Context, column string, value any, query any, args ...any) error {
	return GetDBWithCtx(ctx).Model(new(T)).Where(query, args...).Update(column, value).Error
}

func GetCount[T any](ctx context.Context, query any, args ...any) (int64, error) {
	var count int64
	err := GetDBWithCtx(ctx).Model(new(T)).Where(query, args).Count(&count).Error
	return count, err
}
