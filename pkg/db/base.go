package db

import (
	"github.com/samber/lo"
	"gorm.io/gorm"
	"time"
)

func LimitOffset(limit, page int) (int, int) {
	offset := (page - 1) * limit
	offset = lo.Ternary(limit > 0 && page > 0, offset, 0)
	limit = lo.Ternary(limit > 0 && page > 0, limit, -1)
	return limit, offset
}

type GormModel struct {
	ID        uint64         `gorm:"primarykey" json:"id"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deleted_at"`
}
