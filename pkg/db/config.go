package db

import (
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
)

func NewMysql(dsn string) *gorm.DB {
	return NewMysqlWithLevel(dsn, logger.Warn)
}

func NewMysqlWithLevel(dsn string, level logger.LogLevel) *gorm.DB {
	return mustDB(gorm.Open(mysql.New(mysql.Config{
		DSN:                      dsn,
		DefaultStringSize:        256,
		DisableDatetimePrecision: true,
	}), &gorm.Config{
		Logger: customLogger(level),
	}))
}

func NewPostgres(dsn string) *gorm.DB {
	return NewPostgresWithLevel(dsn, logger.Warn)
}

func NewPostgresWithLevel(dsn string, level logger.LogLevel) *gorm.DB {
	return mustDB(gorm.Open(postgres.New(postgres.Config{
		DSN:                  dsn,
		PreferSimpleProtocol: true, // disables implicit prepared statement usage
	}), &gorm.Config{
		Logger: customLogger(level),
	}))
}

func mustDB(db *gorm.DB, err error) *gorm.DB {
	if err != nil {
		log.Fatalf("failed to connect database: %v", err)
	}
	if err = db.Callback().Query().Before("gorm:query").Register("disable_raise_record_not_found", MaskNotDataError); err != nil {
		log.Fatalf("failed to register callback: %v", err)
	}
	return db
}

func MaskNotDataError(gormDB *gorm.DB) {
	gormDB.Statement.RaiseErrorOnNotFound = false
}

func customLogger(level logger.LogLevel) logger.Interface {
	return logger.New(log.New(os.Stdout, "\r\n", log.LstdFlags), logger.Config{
		SlowThreshold: time.Second,
		LogLevel:      level,
		Colorful:      true,
	})
}

func AutoCreateTable(gdb *gorm.DB, dst ...any) {
	//table must is  schema.table
	for _, v := range dst {
		if err := gdb.AutoMigrate(v); err != nil {
			continue
		}
	}
}
