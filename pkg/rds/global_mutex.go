package rds

import (
	"fmt"
	"github.com/go-redsync/redsync/v4"
	"github.com/go-redsync/redsync/v4/redis/goredis/v9"
	"github.com/redis/go-redis/v9"
	"gitlab.com/go-lib1/go-common/pkg/utils"
)

var (
	mutexMap = utils.NewMap[string, *Mutex]()
	rs       *redsync.Redsync
)

type Mutex struct {
	mutex *redsync.Mutex
}

func newMutex(rds *redsync.Mutex) *Mutex {
	return &Mutex{rds}
}

func (m *Mutex) Lock() error {
	if m == nil {
		return fmt.Errorf("mutex is nil")
	}
	return m.mutex.Lock()
}

func (m *Mutex) Unlock() (bool, error) {
	if m == nil {
		return false, fmt.Errorf("mutex is nil")
	}
	return m.mutex.Unlock()
}

func InitRedisSync(c *redis.Client) {
	pool := goredis.NewPool(c)
	rs = redsync.New(pool)
}

func SetRdsGlobalLockName(name string) {
	rdsMutex := rs.NewMutex(name)
	mutexMap.Store(name, newMutex(rdsMutex))
}

func SetRdsGlobalLockNameWithOption(name string, options ...redsync.Option) {
	rdsMutex := rs.NewMutex(name, options...)
	mutexMap.Store(name, newMutex(rdsMutex))
}

func GetRdsGlobalLock(name string) *Mutex {
	if mutex, ok := mutexMap.Load(name); ok {
		return mutex
	}
	return nil
}

func GlobalLock(name string) *Mutex {
	return GetRdsGlobalLock(name)
}
