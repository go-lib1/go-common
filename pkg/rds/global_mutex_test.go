package rds

import (
	"github.com/go-redsync/redsync/v4"
	"testing"
	"time"
)

func Test_mutex(t *testing.T) {
	SetRdsGlobalLockNameWithOption("test",
		redsync.WithExpiry(time.Second),
		redsync.WithTries(3))
}
