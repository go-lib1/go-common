package rds

import (
	"github.com/redis/go-redis/v9"
	"log"
)

func NewRedis(dsn string) *redis.Client {
	opt, err := redis.ParseURL(dsn)
	if err != nil {
		log.Fatalf("failed to connect redis: %v", err)
	}

	return redis.NewClient(opt)
}

func NewRedisWithOpt(dsn string, f func(options *redis.Options)) *redis.Client {
	opt, err := redis.ParseURL(dsn)
	if err != nil {
		log.Fatalf("failed to connect redis: %v", err)
	}
	f(opt)
	return redis.NewClient(opt)
}
