package rds

import (
	"github.com/redis/go-redis/v9"
)

var _rds *redis.Client

func SetRds(rds *redis.Client) {
	_rds = rds
}

func GetRds() *redis.Client {
	return _rds
}


