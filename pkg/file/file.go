package file

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
)

func DeleteFile(path string) error {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			return fmt.Errorf("file not exist")
		}
		return err
	}

	if err := os.Remove(path); err != nil {
		return err
	}

	return nil
}

func DeleteFiles(paths ...string) error {
	for _, v := range paths {
		if err := DeleteFile(v); err != nil {
			return err
		}
	}
	return nil
}

func CurrentDir() string {
	// 获取当前文件的绝对路径
	_, filename, _, _ := runtime.Caller(0)
	// 获取当前文件所在的目录
	return filepath.Dir(filename)
}
