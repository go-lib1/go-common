package httpx

import (
	"github.com/gin-gonic/gin"
	"github.com/gogf/gf/v2/util/gconv"
	"net/http"
)

func CtxJsonAll(ctx *gin.Context, httpCode, code int, msg string, data any) {
	if err, ok := data.(error); ok {
		data = gconv.String(err)
		code = 500_000
	}
	ctx.JSON(httpCode, gin.H{
		"code": code,
		"msg":  msg,
		"data": data,
	})
}

func Ok(ctx *gin.Context, code int, msg string, data any) {
	CtxJsonAll(ctx, http.StatusOK, code, msg, data)
}

func Ok200(ctx *gin.Context, data any) {
	CtxJsonAll(ctx, http.StatusOK, http.StatusOK, "success", data)
}

func Err(ctx *gin.Context, httpCode, code int, msg string) {
	CtxJsonAll(ctx, httpCode, code, msg, nil)
}

func ErrCode(ctx *gin.Context, code int, msg string) {
	CtxJsonAll(ctx, code, code, msg, nil)
}

func Err200(ctx *gin.Context, code int, msg string) {
	CtxJsonAll(ctx, http.StatusOK, code, msg, nil)
}

type Rsp[T any] struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data T      `json:"data"`
}

type Rsp1[T any] struct {
	Code string `json:"code"`
	Msg  string `json:"msg"`
	Data T      `json:"data"`
}
