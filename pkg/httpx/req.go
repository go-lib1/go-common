package httpx

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gogf/gf/v2/util/gconv"
	"io"
	"moul.io/http2curl"
	"net/http"
	"time"
)

const (
	GET    = "GET"
	POST   = "POST"
	PUT    = "PUT"
	DELETE = "DELETE"
)

func NewClient() *http.Client {
	return &http.Client{}
}

var defaultClient = &http.Client{}

func RequestWithCtxCode(c *http.Client, method, url string, header map[string]string, body any, timeout time.Duration, isLog, isCurl bool) (int, []byte, error) {
	rsp, command, err := requestWithCtx(c, method, url, header, body, timeout, false, isLog, isCurl)
	return requestWithResult(rsp, command, err, isCurl)
}

func RequestWithCtx(c *http.Client, method, url string, header map[string]string, body any, timeout time.Duration, isLog, isCurl bool) ([]byte, error) {
	rsp, command, err := requestWithCtx(c, method, url, header, body, timeout, false, isLog, isCurl)
	_, bytes, err := requestWithResult(rsp, command, err, isCurl)
	return bytes, err
}

func RequestWithTimeout(c *http.Client, method, url string, header map[string]string, body any, timeout time.Duration, isLog, isCurl bool) ([]byte, error) {
	rsp, command, err := requestWithTimeout(c, method, url, header, body, timeout, false, isLog, isCurl)
	_, bytes, err := requestWithResult(rsp, command, err, isCurl)
	return bytes, err
}

func RequestWithCtxCurl(c *http.Client, method, url string, header map[string]string, body any, timeout time.Duration, isLog, isCurl bool) ([]byte, *http2curl.CurlCommand, error) {
	rsp, command, err := requestWithCtx(c, method, url, header, body, timeout, false, isLog, isCurl)
	_, bytes, err := requestWithResult(rsp, command, err, isCurl)
	return bytes, command, err
}

func RequestWithTimeoutCurl(c *http.Client, method, url string, header map[string]string, body any, timeout time.Duration, isLog, isCurl bool) ([]byte, *http2curl.CurlCommand, error) {
	rsp, command, err := requestWithTimeout(c, method, url, header, body, timeout, false, isLog, isCurl)
	_, bytes, err := requestWithResult(rsp, command, err, isCurl)
	return bytes, command, err
}

func requestWithTimeout(c *http.Client, method, url string, header map[string]string, body any, timeout time.Duration, isCloseBody, isLog, isCurl bool) (rsp *http.Response, command *http2curl.CurlCommand, err error) {
	return requestWithBase(c, method, url, header, body, timeout, isCloseBody, isLog, isCurl, func(req *http.Request) (*http.Response, error) {
		return c.Do(req)
	})
}

func requestWithResult(rsp *http.Response, command *http2curl.CurlCommand, err error, isCurl bool) (int, []byte, error) {
	if err != nil {
		if isCurl {
			err = fmt.Errorf("%v, curl: %+v", err, command)
		}
		return 0, nil, err
	}

	if rsp != nil && rsp.Body != nil {
		defer rsp.Body.Close()

		b, err := io.ReadAll(rsp.Body)
		if err != nil && isCurl {
			err = fmt.Errorf("%v, curl: %+v", err, command)
			return 0, nil, err
		}

		if rsp.StatusCode != http.StatusOK {
			err = fmt.Errorf("http code code: %v, body: %v", rsp.StatusCode, gconv.String(b))
			return rsp.StatusCode, nil, err
		}
		return rsp.StatusCode, b, err
	}

	return 0, nil, fmt.Errorf("err and resp is nil")
}

func requestWithCtx(c *http.Client, method, url string, header map[string]string, body any, timeout time.Duration, isCloseBody, isLog, isCurl bool) (rsp *http.Response, command *http2curl.CurlCommand, err error) {
	return requestWithBase(c, method, url, header, body, timeout, isCloseBody, isLog, isCurl, func(req *http.Request) (*http.Response, error) {
		if timeout == 0 {
			timeout = time.Minute
		}
		ctx, cancel := context.WithTimeout(req.Context(), timeout)
		defer cancel()

		return c.Do(req.WithContext(ctx))
	})
}

func requestWithBase(c *http.Client, method, url string, header map[string]string, body any, timeout time.Duration, isCloseBody, isLog, isCurl bool, f func(r *http.Request) (*http.Response, error)) (rsp *http.Response, command *http2curl.CurlCommand, err error) {
	var errStr string
	defer func() {
		if err != nil && isLog {
			fmt.Println(errStr)
		}
	}()

	req, command, err := baseReq(c, method, url, header, body, timeout, isCloseBody, isLog, isCurl)
	if err != nil {
		return nil, command, err
	}

	resp, err := f(req)
	if err != nil {
		return nil, command, err
	}

	if isCloseBody {
		defer resp.Body.Close()
	}

	return resp, command, nil
}

func baseReq(c *http.Client, method, url string, header map[string]string, body any, timeout time.Duration, isCloseBody, isLog, isCurl bool) (*http.Request, *http2curl.CurlCommand, error) {
	if c == nil {
		c = defaultClient
	}

	if timeout == 0 {
		timeout = time.Minute
	}
	c.Timeout = timeout

	var bodyBytes *bytes.Buffer
	if body == nil {
		body = map[string]any{}
	}

	if byt, err := json.Marshal(body); err != nil {
		return nil, nil, err
	} else {
		bodyBytes = bytes.NewBuffer(byt)
	}

	req, err := http.NewRequest(method, url, bodyBytes)
	if err != nil {
		return nil, nil, err
	}

	req.Header.Set("content-type", "application/json")
	if header != nil {
		for k, v := range header {
			req.Header.Add(k, v)
		}
	}

	var command *http2curl.CurlCommand
	if isCurl {
		command, _ = http2curl.GetCurlCommand(req)
	}

	return req, command, err
}
