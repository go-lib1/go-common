package httpx

import (
	"fmt"
	"moul.io/http2curl"
	"net/http"
	"testing"
)

func Test_req(t *testing.T) {
	if req, err := http.NewRequest("GET", "https://testnet.fufi.exchange/api/market/ticker/?exchange=binance&symbol=btc.usdt", nil); err != nil {
		fmt.Println("http error: ", err)
	} else if command, err := http2curl.GetCurlCommand(req); err != nil {
		fmt.Println("command error", err)
	} else {
		fmt.Println(command)
	}
}

func TestRequestWithReqTimeout(t *testing.T) {
	bytes, command, err := RequestWithTimeoutCurl(NewClient(), GET, "https://testnet.fufi.exchange/api/market/ticker/?exchange=binance&symbol=btc.usdt", nil, nil, 0, false, true)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(command)
	fmt.Println(string(bytes))
}
