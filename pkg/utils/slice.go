package utils

import (
	"github.com/samber/lo"
)

func NotEmpty[T any](arr []*T) []*T {
	return lo.Filter(arr, func(item *T, index int) bool { return item != nil })
}

func PtrSliceToSlice[T any](ptrArr []*T) []T {
	if len(ptrArr) == 0 {
		return nil
	}
	return lo.Map(NotEmpty(ptrArr), func(item *T, _ int) T {
		return *item
	})
}