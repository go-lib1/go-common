package utils

import (
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/samber/lo"
)

func As[T any](val any) (T, bool) {
	t, ok := val.(T)
	return t, ok
}

func To[T any](param any) (*T, error) {
	t := new(T)
	err := gconv.Struct(param, &t)
	return t, err
}

func ToSlice[T any](params []any) ([]T, error) {
	var ts []T
	err := gconv.SliceStruct(params, &ts)
	return ts, err
}

func ToS[T any](params any) ([]T, error) {
	var ts []T
	err := gconv.SliceStruct(params, &ts)
	return ts, err
}

func SlicePtrToStruct[T any](arr []*T) []T {
	return lo.Map(NotEmpty(arr), func(item *T, index int) T {
		return *item
	})
}
