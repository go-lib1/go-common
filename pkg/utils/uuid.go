package utils

import (
	"github.com/gogf/gf/v2/util/gconv"
	guuid "github.com/google/uuid"
	"github.com/samber/lo"
	uuid "github.com/satori/go.uuid"
)

func GenUUID() string {
	return uuid.NewV4().String()
}

func GenUUID4() string {
	return uuid.NewV4().String()
}

func GenNum(v int) string {
	if v <= 0 {
		if t, err := guuid.NewRandom(); err != nil {
			return gconv.String(t.ID())
		} else {
			return ""
		}
	}
	r := ""
	for i := 0; i < v; i++ {
		if t, err := guuid.NewRandom(); err == nil {
			r += gconv.String(t.ID())
		} else {
			return ""
		}
	}

	if len(r) < v*10 {
		r += lo.RandomString(v*10-len(r), lo.NumbersCharset)
	}

	return r
}
