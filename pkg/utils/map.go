package utils

import (
	"sync"
)

type Map[K, V any] struct {
	m sync.Map
}

func NewMap[K, V any]() *Map[K, V] {
	return &Map[K, V]{}
}

func (m *Map[K, V]) Load(key K) (value V, ok bool) {
	v, ok := m.m.Load(key)
	if !ok {
		return
	}
	value, ok = v.(V)
	return
}

func (m *Map[K, V]) Store(key K, value V) {
	m.m.Store(key, value)
}

func (m *Map[K, V]) Delete(key K) {
	m.m.Delete(key)
}

func (m *Map[K, V]) Range(f func(key K, value V) bool) {
	m.m.Range(func(k, v any) bool {
		return f(k.(K), v.(V))
	})
}

func (m *Map[K, V]) RangeTrue(f func(key K, value V)) {
	m.m.Range(func(k, v any) bool {
		f(k.(K), v.(V))
		return true
	})
}

func (m *Map[K, V]) LoadOrStore(key K, value V) (actual V, loaded bool) {
	v, ok := m.m.LoadOrStore(key, value)
	if !ok {
		return v.(V), false
	}
	return v.(V), true
}

func (m *Map[K, V]) LoadAndDelete(key K) (value V, loaded bool) {
	v, ok := m.m.LoadAndDelete(key)
	if !ok {
		return value, false
	}
	return v.(V), true
}

func (m *Map[K, V]) Len() int {
	count := 0
	m.m.Range(func(_, _ interface{}) bool {
		count++
		return true
	})
	return count
}

// MapAddMap 合并两个map
func MapAddMap[K comparable, V any](map1, map2 map[K]V) map[K]V {
	result := make(map[K]V)
	for k, v := range map1 {
		result[k] = v
	}
	for k, v := range map2 {
		if _, ok := result[k]; !ok {
			result[k] = v
		}
	}
	return result
}

func MapRelateMap[K comparable, V any, V1 any, V2 any](map1 map[K]V, map2 map[K]V1, f func(v V, v1 V1) V2) map[K]V2 {
	result := make(map[K]V2)
	for k, v := range map1 {
		if v1, ok1 := map2[k]; ok1 {
			result[k] = f(v, v1)
		}
	}
	return result
}

func MapRelateMapWithKey[K comparable, V any, V1 any, V2 any](map1 map[K]V, map2 map[K]V1, f func(k K, v V, v1 V1) V2) map[K]V2 {
	result := make(map[K]V2)
	for k, v := range map1 {
		if v1, ok1 := map2[k]; ok1 {
			result[k] = f(k, v, v1)
		}
	}
	return result
}
