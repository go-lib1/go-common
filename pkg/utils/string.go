package utils

import (
	"github.com/gogf/gf/v2/text/gstr"
	"gitlab.com/go-lib1/go-common/pkg/log"
)

func PrintStar(msg string, count int, str string) {
	if count == 0 {
		count = 20
	}
	if len(str) == 0 {
		str = "*"
	}
	countStr := gstr.Repeat(str, count)
	log.Log().Infof("%s  %s  %s", countStr, msg, countStr)
}
