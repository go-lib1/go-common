package cryptx

import (
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/text/unicode/norm"
)

func Hash(password string) (string, error) {
	pw := norm.NFKC.String(password)

	hash, err := bcrypt.GenerateFromPassword([]byte(pw), 12)
	if err != nil {
		return "", err
	}

	return string(hash), nil
}

func ValidateHash(password string, checkedHash []byte) bool {
	pw := norm.NFKC.String(password)

	err := bcrypt.CompareHashAndPassword(checkedHash, []byte(pw))
	return err == nil
}
