package auth

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/samber/lo"
	"strings"
	"time"
)

var (
	key    = []byte(lo.RandomString(32, lo.AllCharset))
	expire = time.Hour * 24 * 7
)

func SetKey(k string) {
	key = []byte(k)
}

func SetExpire(e time.Duration) {
	expire = e
}

type CustomClaims[T any] struct {
	Value T
	jwt.StandardClaims
}

func NewCustomClaims[T any](value T) *CustomClaims[T] {
	return &CustomClaims[T]{
		Value: value,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(expire).Unix(),
		},
	}
}

// Decode a token string into a token object
func Decode[T any](tokenString string) (*CustomClaims[T], error) {
	token, err := jwt.ParseWithClaims(tokenString, new(CustomClaims[T]), func(token *jwt.Token) (interface{}, error) {
		return key, nil
	})
	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(*CustomClaims[T])
	if ok && token.Valid {
		return claims, nil
	} else {
		return nil, fmt.Errorf("token claims failed")
	}
}

// Encode a claim into a JWT
func Encode[T any](claims *CustomClaims[T]) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(key)
}

func Refresh[T any](token string, seconds int64) (string, error) {
	claims, err := Decode[T](token)
	if err != nil {
		return "", err
	}
	claims.ExpiresAt = time.Now().Unix() + seconds
	return Encode(claims)
}

func Valid[T any](token string) (bool, error) {
	claims, err := Decode[T](token)
	if err != nil {
		return false, err
	}
	return time.Now().Unix() < claims.ExpiresAt, nil
}

func EncodeByCtx[T any](c *gin.Context) (*CustomClaims[T], error) {
	return Decode[T](GetToken(c))
}

func GetToken(c *gin.Context) string {
	token := c.GetHeader("Authorization")

	if token == "" {
		token = c.Request.FormValue("token")
	}

	if len(strings.Split(token, " ")) > 1 {
		token = strings.Split(token, " ")[1]
	}
	return token
}
