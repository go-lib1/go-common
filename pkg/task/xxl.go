package task

import (
	"fmt"
	"github.com/xxl-job/xxl-job-executor-go"
	"log"
)

func XxlTask(addr, token, ip, port, key string, taskMap map[string]xxl.TaskFunc) {
	exec := xxl.NewExecutor(
		xxl.ServerAddr(addr),
		xxl.AccessToken(token),   //请求令牌(默认为空)
		xxl.ExecutorIp(ip),       //可自动获取
		xxl.ExecutorPort(port),   //默认9999（非必填）
		xxl.RegistryKey(key),     //执行器名称
		xxl.SetLogger(&logger{}), //自定义日志
	)
	exec.Init()
	//设置日志查看handler
	exec.LogHandler(func(req *xxl.LogReq) *xxl.LogRes {
		return &xxl.LogRes{Code: xxl.SuccessCode, Msg: "", Content: xxl.LogResContent{
			FromLineNum: req.FromLineNum,
			ToLineNum:   2,
			LogContent:  "这个是自定义日志handler",
			IsEnd:       true,
		}}
	})
	//注册任务handler
	for name, taskFunc := range taskMap {
		exec.RegTask(name, taskFunc)
	}
	log.Fatal(exec.Run())
}

// xxl.Logger接口实现
type logger struct{}

func (l *logger) Info(format string, a ...interface{}) {
	fmt.Println(fmt.Sprintf("自定义日志 - "+format, a...))
}

func (l *logger) Error(format string, a ...interface{}) {
	log.Println(fmt.Sprintf("自定义日志 - "+format, a...))
}
