package image

import (
	"github.com/gen2brain/go-fitz"
	"image/jpeg"
	"os"
)

func CreateJpg(source, output string) error {
	doc, err := fitz.New(source)
	if err != nil {
		return err
	}
	defer doc.Close()

	for n := 0; n < doc.NumPage(); n++ {
		if err = create(doc, n, output); err != nil {
			return err
		}
	}
	return nil
}

func create(doc *fitz.Document, n int, output string) error {
	img, err := doc.Image(n)
	if err != nil {
		return err
	}

	f, err := os.Create(output)
	if err != nil {
		return err
	}
	defer f.Close()

	err = jpeg.Encode(f, img, &jpeg.Options{jpeg.DefaultQuality})
	if err != nil {
		return err
	}

	return nil
}
