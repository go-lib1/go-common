package ginx

import (
	"github.com/gin-gonic/gin"
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/samber/lo"
	"io"
)

func Get[T any](c *gin.Context, key string) (T, bool) {
	if cb, ok := c.Get(key); ok {
		if val, ok := cb.(T); ok {
			return val, ok
		}
	}

	return lo.Empty[T](), false
}

func GetOrEmpty[T any](c *gin.Context, key string) T {
	if val, ok := Get[T](c, key); ok {
		return val
	}

	return lo.Empty[T]()
}

func BodyString(c *gin.Context) string {
	return string(GetOrEmpty[[]byte](c, gin.BodyBytesKey))
}

func GetAction(c *gin.Context) {
	paramMap := ParamsToMap(c)
	c.Set(gin.BodyBytesKey, gconv.Bytes(paramMap))
}

func ParamsToMap(c *gin.Context) map[string]any {
	paramMap := make(map[string]any)
	//query
	for key, val := range c.Request.URL.Query() {
		if len(val) == 1 {
			paramMap[key] = val[0]
		} else {
			paramMap[key] = val
		}
	}

	//body
	body, err := io.ReadAll(c.Request.Body)
	if err != nil {
		return paramMap
	}
	for key, val := range gconv.Map(body) {
		paramMap[key] = val
	}

	return paramMap
}
