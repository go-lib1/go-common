package reflectx

import (
	"fmt"
	"reflect"
)

func StructToHeader(data any, val string) [][]string {
	if data == nil {
		return [][]string{{val}}
	}
	productType := reflect.TypeOf(data)
	productName := productType.Name()
	return [][]string{{productName}}
}

func StructsToContent(data any) [][]string {
	if data == nil {
		return nil
	}

	value := reflect.ValueOf(data)
	if value.Kind() != reflect.Slice {
		return nil
	}

	var contents [][]string

	// 获取字段名
	typeOfElem := value.Type().Elem()
	var fieldNames []string
	for i := 0; i < typeOfElem.NumField(); i++ {
		fieldNames = append(fieldNames, typeOfElem.Field(i).Name)
	}
	contents = append(contents, fieldNames)

	// 获取字段值
	for i := 0; i < value.Len(); i++ {
		var row []string
		elem := value.Index(i)
		for j := 0; j < elem.NumField(); j++ {
			field := elem.Field(j)
			row = append(row, fmt.Sprintf("%v", field.Interface()))
		}
		contents = append(contents, row)
	}

	return contents
}
