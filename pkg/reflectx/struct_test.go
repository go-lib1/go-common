package reflectx

import "testing"

func Test_struct(t *testing.T) {
	type ABC struct {
		A string
		B int
		C float64
	}

	abcs := []ABC{
		{"a", 1, 1.1},
		{"b", 2, 2.2},
		{"c", 3, 3.3},
	}

	header := StructToHeader(abcs[0], "")
	t.Log(header)

	body := StructsToContent(abcs)
	t.Log(body)
}
